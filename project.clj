(defproject app "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [compojure "1.4.0"]
                 [korma "0.4.0"]
                 [cheshire "5.5.0"]
                 [com.taoensso/carmine "2.12.1"]
                 [http-kit "2.1.19"]
                 [org.clojure/java.jdbc "0.4.2"]
                 [mysql/mysql-connector-java "5.1.6"]
                 [ring/ring-defaults "0.1.5"]]

  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler app.handler/app}
  :main app.main
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
