(ns app.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [korma.db :refer [mysql defdb]]
            [app.handlers.cards :as cards]
            [app.middlewares.token :refer [token]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]))

(defdb dev
  (mysql
    {:db "pismo",
     :user "root"
     :password ""}))

(defroutes app-routes
  (context "/api/v1" [] cards/router)
  (route/not-found "Not Found"))

(def app
  (token (wrap-defaults app-routes site-defaults)))

