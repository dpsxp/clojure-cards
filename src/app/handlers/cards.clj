(ns app.handlers.cards
  (:require [compojure.core :refer :all]
            [cheshire.core :refer [generate-string]]
            [app.entities.card :as card]))

(def json
  {:headers {"content-type" "application/json"}})

(defn error [body]
   (merge json {:status 404 :body (generate-string {:error body})}))

(defn ok [body]
   (merge json {:status 200 :body (generate-string body)}))

(defn index
  " Handler para trazer todos os cartoes do usuario na org "
  [{org_id :tenant {:keys [limit cuid] :or {limit 10}} :params}]
  (let [result (card/all org_id cuid limit)]
    (ok (seq result))))

(defn update-status
  " Handler para atualizar o status do cartao com base no id, customer_id, e org_id "
  [{tenant :tenant {:keys [id status cuid]} :params}]
  (if (seq (card/find id cuid tenant))
    (do
      (ok {:result (card/update-status id status cuid tenant)}))
    (do
      (error (str "Can't find card with id " id " for customer with id " cuid " inside the tenant " tenant)))))

(defroutes router
  (GET "/customers/:cuid/cards" [] index)
  (GET "/customers/:cuid/cards/:id/update-status" [] update-status))

