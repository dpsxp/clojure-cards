(ns app.main
  (:gen-class)
  (:use [app.handler :only [app]]
        [compojure.handler :only [site]]
        [org.httpkit.server :only [run-server]]))

(defn -main [& args]
  (run-server app {:port 8080}))
