(ns app.entities.card
  (:require [korma.core :refer :all]
            [korma.db :refer [mysql defdb]]))

(defentity cards
  (pk :card_id)
  (table :Cards :card)
  (entity-fields [:card_id :id] [:cardstatus :status] [:cardhash :hash] ))

(def statuses #{"normal" "fraud"})

(defn valid-status? [status]
  (contains? statuses status))

(defn all
  ([org_id cid max]
    (select cards (where {:org_id org_id :customer_id cid})(limit max)))
  ([org_id cid]
    (all org_id cid 10)))

(defn find [id customer_id org_id]
  (select cards
    (where {:org_id org_id :card_id id :customer_id customer_id})
    (limit 1)))

(defn update-status [id status customer_id org_id ]
  (if (valid-status? status)
    (do
      (update cards
        (where {:card_id id :org_id org_id :customer_id customer_id})
        (set-fields {:cardstatus status}))
      (nth (find id customer_id org_id) 0))
    (do "Status inválido")))


