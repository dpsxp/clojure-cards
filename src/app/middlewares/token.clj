(ns app.middlewares.token
  (:require [clojure.string :refer [split]]
            [taoensso.carmine :as car :refer (wcar)]))

(defn token [app]
  (def redis {})
  (defmacro wcar* [& body] `(car/wcar redis ~@body))

  (defn refresh-token
    [token]
    (wcar*
      (car/set "token" token)
      (car/expire "token" 10)))

  (fn [{{authorization "authorization"} :headers :as req}]
    (def token (nth (split authorization #" ") 1 nil))
    (validate token))
    (if (seq token)
      (do
        (refresh-token token)
        (app (assoc req :tenant token)))
      (do
        {:status 401 :body "Missing token"}))))

